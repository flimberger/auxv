#include <stdio.h>

#define NAUX		38

/* These values come from the Linux source code include/uapi/linux/auxvec.h */
#define AT_NULL		0
#define AT_IGNORE	1
#define AT_EXECFD	2
#define AT_PHDR		3
#define AT_PHENT	4
#define AT_PHNUM	5
#define AT_PAGESZ	6
#define AT_BASE		7
#define AT_FLAGS	8
#define AT_ENTRY	9
#define AT_NOTELF	10
#define AT_UID		11
#define AT_EUID		12
#define AT_GID		13
#define AT_EGID		14
#define AT_PLATFORM	15
#define AT_HWCAP	16
#define AT_CLKTCK	17
#define AT_SECURE	23
#define AT_BASE_PLATFORM	24
#define AT_RANDOM	25
#define AT_HWCAP2	26
#define AT_EXECFN	31
#define AT_MINSIGSTKSZ	51

/* i386-only */
#define AT_SYSINFO	32

#define AT_SYSINFO_EHDR	33

enum vtype {
	PTR = 0,
	STR,
	NUM,
};

static struct auxeinfo {
	size_t		 num;
	char		*name;
	enum vtype	 type;
} auxeinfos[] = {
	{AT_NULL,	"end of vector"},
	{AT_IGNORE,	"empty"},
	{AT_EXECFD,	"program file descriptor", NUM},
	{AT_PHDR,	"program headers"},
	{AT_PHENT,	"size of program header entry", NUM},
	{AT_PHNUM,	"number of program headers", NUM},
	{AT_PAGESZ,	"system page size", NUM},
	{AT_BASE,	"interpreter base address"},
	{AT_FLAGS,	"flags"},
	{AT_ENTRY,	"entrypoint"},
	{AT_NOTELF,	"program is not ELF", NUM},
	{AT_UID,	"real UID", NUM},
	{AT_EUID,	"effective UID", NUM},
	{AT_GID,	"real GID", NUM},
	{AT_EGID,	"effective GID", NUM},
	{AT_PLATFORM,	"CPU architecture", STR},
	{AT_HWCAP,	"CPU capabilities"},
	{AT_CLKTCK,	"clock tick frequency", NUM},
	{AT_SECURE,	"secure mode", NUM},
	{AT_BASE_PLATFORM,	"real CPU architecture", STR},
	{AT_RANDOM,	"random"},
	{AT_HWCAP2,	"CPU capabilities extension"},
	{AT_EXECFN,	"program filename", STR},
};
#define NAUXEINFO	(sizeof(auxeinfos)/sizeof(auxeinfos[0]))

int
main(int argch, char *argv[], char *environ[])
{
	size_t		i, *auxv, j;

	for (i = 0; environ[i]; i++)
		;
	auxv = (void*)(environ + i + 1);
	for (i = 0; i < NAUX; i += 2) {
		for (j = 0; j < NAUXEINFO; j++)
			if (auxeinfos[j].num == *(auxv + i))
				break;
		if (j == NAUXEINFO) {
			printf("[%zu]:\t0x%012zx\n", *(auxv + i), *(auxv + i + 1));
			continue;
		}
		switch (auxeinfos[j].type) {
		case PTR:
			printf("%s:\t0x%012zx\n", auxeinfos[j].name, *(auxv + i + 1));
			break;
		case STR:
			printf("%s:\t%s\n", auxeinfos[j].name, *(char **)(auxv + i + 1));
			break;
		case NUM:
			printf("%s:\t%12zu\n", auxeinfos[j].name, *(auxv + i + 1));
			break;
		}
	}
}
