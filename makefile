CC=clang
CFLAGS=-std=c99 -Wall
OFILES=pauxv.o

.PHONY:	default clean
default:	pauxv
pauxv:	pauxv.o
	$(CC) $(LDFLAGS) -o pauxv pauxv.o
clean:
	rm -f $(OFILES) pauxv
